# runner

## Instructions

copy `daemon.json` to `/etcd/docker/daemon.json`. This enables [containerd](https://docs.docker.com/engine/storage/containerd/) for image storage, which in turn allows for multi-arch image builds to be built in one step, cached and then pushed in a seperate job.


